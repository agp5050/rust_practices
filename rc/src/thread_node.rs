use std::borrow::Borrow;
/// Arc test ;
use std::sync::atomic;
use std::sync::Arc;
use std::thread::spawn;

pub fn test_thread() {
    let arr = vec![1];
    let handle = std::thread::spawn(move || {
        println!("{:?}", arr);
        "done"
    });
    println!("thread return value:{}", handle.join().unwrap())
}

pub fn test_multi_thread_share() {
    let s = "aaa";
    println!("s addr:{:p}", s);
    println!("s addr:{:p}", *&s);
    let s1 = Arc::new(s).clone();
    println!("s1:{}", s1);
    spawn(move || println!("s1:{},current thread name:{:?}", s1,std::thread::current().name().unwrap())); //s1 有了static 生命周期。
}
