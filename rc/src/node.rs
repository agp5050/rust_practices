use std::cell::RefCell;
use std::rc::Rc;
#[derive(Debug)]
pub struct Entry {
    id: usize,
    next: Option<Rc<RefCell<Entry>>>,
}

impl Entry {
    fn new(id: usize) -> Self {
        Self { id, next: None }
    }
    fn getNext(&mut self) -> Option<Rc<RefCell<Entry>>> {
        self.next.as_ref().map(|x:&Rc<RefCell<Entry>>| x.clone()) //return next。
    }
    fn setNext(&mut self, next: Rc<RefCell<Entry>>) {
        self.next = Some(next);
    }
}

pub fn test_entry() {
    let mut node1 = Entry::new(1);
    let mut node2 = Entry::new(2);
    let mut node3 = Entry::new(3);
    let node4 = Entry::new(4);
    node3.setNext(Rc::new(RefCell::new(node4)));
    let rcNode3=Rc::new(RefCell::new(node3));
    println!("{:p}",* &rcNode3);//解引用 指向引用的堆地址
    node1.setNext(rcNode3);
    let node1Next=node1.getNext().unwrap();
    println!("{:p}",* &node1Next);  //解引用 指向引用的堆地址
    node2.setNext(node1Next);
    println!("node2 through setNext \n {:?}",node2);

    let node5=Entry::new(5);
    let node3=node1.getNext().unwrap();

    // 获得可变引用，来修改 downstream；不用通过方法setNext去修改了。
    node3.borrow_mut().next=Some(Rc::new(RefCell::new(node5)));
    println!("node3 add next node5 through borrow_mut: \n {:?}",node3);

}

pub fn test_br(){
    let ref a;  //表示引用类型
    a=&1; //& 表示引用地址
    println!("{:p}",a);

    let ref b=2;  //&i32  ,此方式相当于将2的&地址赋予b
    let c=&2; //&i32
    println!("b addr:{:p}",b);
    println!("c addr:{:p}",c);

}