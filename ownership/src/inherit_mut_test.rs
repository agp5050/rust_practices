// 定义父结构体Animal
#[derive(Debug)]
struct Animal {
    age: u16,
}
impl Animal {
    fn new(age: u16) -> Self {
        Self { age }
    }
}
impl Animal {
    pub fn print_age(&self) {
        println!("Animal {}", self.age);
    }

    fn set_age(&mut self, age: u16) {
        self.age = age;
    }
}

// 定义子类
#[derive(Debug)]
///组合优于继承
struct Dog  {
    animal: Animal,
    name: String,
}
impl Dog  {
    fn new(animal: Animal, name: &str) -> Self {
        Self { animal , name: name.to_string()}
    }
}
impl Dog {
    fn as_animal(&self) -> &Animal {
        &self.animal
    }

    fn as_mut_animal(&mut self) -> &mut Animal {
        &mut self.animal
    }
}

// 调用
#[test]
///rust其实并没有传统意义上的继承
/// 推测可能是rust团队不赞同继承方案，而是觉得组合优于继承，综上其实换一种思维方式也算实现了继承
fn main_test() {
    let student = Animal::new(18);
    let mut tome = Dog ::new(student, "旺财");

    tome.animal.print_age();
    tome.animal.set_age(16);
    tome.animal.print_age();

    println!("{:#?}", tome);

    let a: &Animal = tome.as_animal();
    a.print_age();
    let a: &mut Animal = tome.as_mut_animal();
    a.set_age(6);
    a.print_age();
}
