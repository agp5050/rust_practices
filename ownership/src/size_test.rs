
use std::collections::HashMap;
use std::mem::size_of;

enum E {
    A(f64),
    B(HashMap<String, String>),
    C(Result<Vec<u8>, String>),
}

// 这是一个声明宏，它会打印各种数据结构本身的大小，在 Option 中的大小，以及在 Result 中的大小
macro_rules! show_size {
    (header) => {
        println!(
            "{:<24} {:>4}    {}    {}",
            "Type", "T", "Option<T>", "Result<T, io::Error>"
        );
        println!("{}", "-".repeat(64));
    };
    ($t:ty) => {
        println!(
            "{:<24} {:4} {:8} {:12}",
            stringify!($t),
            size_of::<$t>(),
            size_of::<Option<$t>>(),
            size_of::<Result<$t, std::io::Error>>(),
        )
    };
}
///
/// Type                        T    Option<T>    Result<T, io::Error>
// ----------------------------------------------------------------
// u8                          1        2           24
// f64                         8       16           24
// &u8                         8        8           24
// Box<u8>                     8        8           24
// &[u8]                      16       16           24
// String                     24       24           32
// Vec<u8>                    24       24           32
// HashMap<String, String>    48       48           56
// E                          56       56           64

pub fn test_size() {
    show_size!(header);
    show_size!(u8);
    show_size!(f64);
    show_size!(&u8);
    show_size!(Box<u8>);
    show_size!(&[u8]); //3 个 word 的胖指针 ,3*8=24 ,一个指向堆内存的指针pointer(8byte:64bit);capacity(8byte:64bit 2^64);length(8byte)

    show_size!(String); //&[u8]
    show_size!(Vec<u8>);
    show_size!(HashMap<String, String>);
    show_size!(E);
}