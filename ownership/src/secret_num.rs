use rand::Rng;
use std::cmp::Ordering;
pub fn test_secret() {
    const MAX_LIMIT: i32 = 10;
    let mut i = 0;
    loop {
        println!("Please input the secret num:");
        let mut line = String::new();
        std::io::stdin().read_line(&mut line).expect("input error");
        println!("Your secret num is:{}", line);
        let gen = rand::thread_rng().gen_range(1..101);
        println!("generated num:{}", gen);
        let pars_rst: u32 = match line.trim().parse() {
            Ok(num) => num,
            Err(err) => continue,
        };

        match pars_rst.cmp(&gen) {
            Ordering::Equal => {
                println!("You get the right number.");
                break;
            }
            Ordering::Greater => println!("too big "),
            Ordering::Less => println!("too small "),
        }
        i += 1;
        if i > MAX_LIMIT {
            break;
        }
    }
}
