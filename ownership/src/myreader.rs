use std::fs::File;
use std::io::{BufReader, Read, Result};
/// 定义一个带有泛型参数 R 的 reader，此处我们不限制 R
pub struct MyReader<R>{
    reader:R,
    buf:String,
}
/// 实现 new 函数时，我们不需要限制 R
impl <R> MyReader<R>{
    pub fn new(reader:R)->Self {
        Self{reader,buf:String::with_capacity(1024),}
    }
}

impl <R> MyReader<R> where R:Read{
    pub  fn process(&mut self) -> Result<usize>{
        self.reader.read_to_string(&mut self.buf)
    }
}