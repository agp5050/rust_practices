use std::ops::Index;
use std::mem::{size_of,align_of};
pub fn first(str:&str) ->&str{
    let trim=str.trim();
    let  a = trim.find(' ');
    match a {
        Some(a) => &trim[..a],
        _ => "",
    }
}

pub fn strtok<'a,'b>(s:&'a mut &str,delimiter:&'b str)->&'a str{
    if let Some(p) = s.find(delimiter){
        let prefix= &s[..p];
        let mut suffix = &s[(p+delimiter.len())..];
        *s=suffix;
        prefix
    }else{
        let prefix= &s[..];
        *s="";
        prefix
    }
}

struct Employee<'a,'b>{
    name:&'a str,
    age:u8,
    title:&'b str,
}
#[derive(Debug)]
pub(crate) struct S1{
    pub(crate) a:u8,
    pub b:u8,
    pub c:u16,
}
struct S2{
    a:u8,
    b:u16,
    c:u8,
}
pub fn print_size(){
    println!("size of S1:{:?},S1:{:?}",size_of::<S1>(),size_of::<S2>());
    println!("size of S1:{:?},S1:{:?}",align_of::<S1>(),align_of::<S2>());
}