use std::fmt::Display;

#[test]
pub fn test1() {
    let a = 255u8;
    println!("{}", a);
    let a16 = 0xf0ff; //0x 16
    println1(a16);
    let a8 = 0o77; //0o 8进制
    println1(a8);
    let b1 = b'A';
    println1(b1);
    let b2 = 0b1111; //0b 2进制；
    println1(b2);
}

pub fn println1<T: Display>(a: T) {
    println!("{}", a);
}

#[test]
pub fn test_loop() {
    let mut count = 0;
    let rst = loop {
        count += 1;
        if count == 10 {
            break count * 2;
        }
    };
    println1(rst)
}
#[test]
pub fn test_for() {
    //不需要判断while(true)条件，以及避免设定错误的true条件。for就更快
    let a = [10, 2, 22, 33];
    for i in a.iter() {
        println1(i);
    }

    for num in (1..4).rev() {
        println1(num);
    }
    println1("liftoff")
}
#[test]
pub fn move_and_clone() {
    let a = String::from("abc");
    let c = String::from("abc");
    let b = a; // a value moved to B.(ptr,len,capacity)
               // println1(a);
    let d = c.clone(); //c.clone 并不会转移c的所有权
    println1(d);
}
#[test]
pub fn copy_move_in_fn_variable() {
    let i = 5;
    copy_variable(i);
    println!("after pass to fn {}", i);
    let str = String::from("aaaa");
    let s3 = receive_and_send_back(str);
    println1(s3);
    // move_string(str);
    // println!("after pass to fn:{}", str); //value moved. incase two variable both release memo. severity.
}

fn copy_variable(v: i32) {
    println1(v);
}
fn move_string(v: String) {
    println1(v);
}

fn receive_and_send_back(str: String) -> String {
    str // return value . also move ownership
}
#[test]
pub fn splice_ref() {
    //字符串的部分引用
    let s = String::from("abc");
    let s1 = &s[0..2];
    println1(s1);
    println1(s1); //reference type is 4 bytes. is copied in stack.
    let s2 = &s[1..];
    println1(s2);
    let s3 = &s[..]; //s3 type of &str
    println1(s3);

    let s4 = "aaaddd"; // s4 &str 是一个切片类型。固定大小，被直接写入到二进制程序中。 指向了二进制程序中的切片位置。
}
